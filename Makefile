RMSDK_VERSION ?= 10.0.4
RMSDK_C_VERSION ?= 10_0
CROSS ?= arm-linux-gnueabihf-
CXX = $(CROSS)g++
CC = $(CROSS)gcc

DEBUG ?= 0
NO_BUILD_OPENSSL ?= 0
INSTALL_DIR ?= $(PWD)/deploy
RMSDK_LIBDIR = $(PWD)/lib/rmsdk/$(RMSDK_VERSION)

OUTPUT_DIR ?= output/$(RMSDK_VERSION)

TARGET_ACSM     = $(OUTPUT_DIR)/acsmdownloader
SRCS_ACSM       = src/acsmdownloader.cpp
TARGET_ACTIVATE = $(OUTPUT_DIR)/activate
SRCS_ACTIVATE   = src/activate.cpp

TARGETS = $(TARGET_ACSM) $(TARGET_ACTIVATE)
CXXFLAGS += -I./include -I./include/rmsdk/ `pkg-config --cflags Qt5Core Qt5Network` -fPIC -DRMSDK_$(RMSDK_C_VERSION) -Wall

ifeq ($(DEBUG),1)
CXXFLAGS += -ggdb
endif

LDFLAGS=-L. -L$(RMSDK_LIBDIR) -L./lib -lrmsdk -lQt5Core -lQt5Network -lpthread

all: $(RMSDK_LIBDIR) $(OUTPUT_DIR) $(TARGETS)

clean:
	rm -rf $(TARGETS)

ultraclean:
	rm -rf output lib tmp $(INSTALL_DIR)

prepare: $(RMSDK_LIBDIR)

install: $(TARGETS)
	rm -rf $(INSTALL_DIR)
	mkdir -p $(INSTALL_DIR)
	cp -r $(RMSDK_LIBDIR) $(INSTALL_DIR)/lib
	cp $(TARGETS) $(INSTALL_DIR)
	cp scripts/acsmdownloader.sh $(INSTALL_DIR)
	cp scripts/activate.sh $(INSTALL_DIR)
	@echo
	@echo "Files installed into $(INSTALL_DIR)"

$(RMSDK_LIBDIR):
	CC=$(CC) NO_BUILD_OPENSSL=$(NO_BUILD_OPENSSL) RMSDK_VERSION=$(RMSDK_VERSION) $(PWD)/scripts/setup.sh

$(OUTPUT_DIR):
	mkdir -p $@

$(TARGET_ACTIVATE): $(SRCS_ACTIVATE)
	$(CXX) $(CXXFLAGS) $^ $(LDFLAGS) -ldl -o $@
$(TARGET_ACSM): $(SRCS_ACSM)
	$(CXX) $(CXXFLAGS) $^ $(LDFLAGS) -o $@

help:
	@echo ""
	@echo "ACSMDownloader Makefile"
	@echo ""
	@echo "Targets :"
	@echo "\tall \t\tDownload external libraries and build all targets (acsmdownloader and activate)"
	@echo "\tclean \t\tClean targets"
	@echo "\tultraclean \tClean targets, lib and install directory"
	@echo "\tprepare \tDownload libraries from Kobo (done by default)"
	@echo "\tinstall \tInstall result"
	@echo ""
	@echo "Main environment variables :"
	@echo "\tCROSS : \tthat defines cross compilation prefix (default \"arm-linux-gnueabihf-\")"
	@echo "\tDEBUG : 1|0 \tto enable debug symbols (default 0)"
	@echo "\tNO_BUILD_OPENSSL : 1|0 \tto disable OpenSSL build and not use the one from Kobo (default 0)"
	@echo "\tINSTALL_DIR : \tdirectory to put everythong needed (default ./deploy)"
	@echo ""
