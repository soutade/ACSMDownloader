ACSMDownloader
--------------

ACSMDownloader is a tool to download _epub_ file from Adobe _acsm_ request file.
It was developed to overcome the lack of Adobe Digital Editions (ADE) software on Linux platforms.
Which is ironic as *ALL* eReaders are based on Linux systems (at least Android/Linux).

ACSMDownloader is the result of a lot of reverse engineering on Kobo _librmsdk.so_ shared library but it's not included with this package as I cannot distribute it without a license from Adobe (that I don't own).

**Warning** : It acts like a client for _librmsdk.so_ which has been built for ARMv7 platform and can only be run on these kind of platform ! Until someone find an x64 version (GNU/Linux compliant) of this library, ACSMDownloader cannot be run on your computer. A solution is to build a web frontend that run on an ARM server.

Current RMSDK version is 10.0.4

**Caution** : This tool was not designed for piracy and any request for this will be ignored.



Compilation
-----------

Simple _make_ will do the job.

Some environment variables can be defined before running _make_ :

  * DEBUG            : 1|0 to enable debug symbols
  * NO_BUILD_OPENSSL : 1 to don't build OpenSSL library (use the one of Kobo or your own)
  * CROSS            : cross compiler prefix (default arm-linux-gnueabihf-)
  * INSTALL_DIR      : default (./deploy)

First step of compilation is to download all libraries in binary form from Kobo update file. It also downloads OpenSSL and compile it because the one from Kobo may crash (compiled with specific target options).

Be careful : Kobo software has been compiled with an old version of Qt Embedded (4.6), but a reference from Qt5 shared library is made. It implies that some functions may not work (especially QString --> std::string conversion).



Adobe files
-----------

ACSMDownloader requires some generated files present in the eReader. These files are _device.xml_, _activation.xml_ and _devicesalt_ (or _devicekey.bin_), they're generally stored into _.adept_ or _adobe-digital-edition_ directory.

The first one describes eReader characteristics (serial, name, RMSDK version...) while the second contains ADEPT certificates and user information, they're easy to find. _devicesalt_/_devkey.bin_ is a base file for ADEPT cryptography and may be harder to find, it may requires root filesystem access via ADB/SSH.

In addition to _acsmdownloader_ binary, an util called _activate_ is delivered. It'll create the necessary _xml_ files and link them to your AdobeID account if you don't want to use the ones from your eReader. Be careful as there is a limited number of devices authorized per Adobe account (5 or 6 in theory, but more than 20 in facts).



Dependencies
------------

  * Qt5



Usage
-----

    ./acsmdownloader [(-d|--device-file) device.xml] [(-a|--activation-file) activation.xml] [(-s|--device-key-file) devicesalt] [(-O|--output-dir) dir] [(-o|--output-file) output.epub] [(-v|--verbose)] [(-h|--help)] (-f|--acsm-file) file.acsm
    
      -d|--device-file		device.xml file from eReader
      -a|--activation-file	activation.xml file from eReader
      -k|--device-key-file	private device key file (eg devicesalt/devkey.bin) from eReader
      -O|--output-dir		Optional output directory were to put result (default ./)
      -o|--output-file		Optional output epub filename (default <title.epub>)
      -f|--acsm-file		ACSM request file for epub download
      -v|--verbose		Increase verbosity, can be set multiple times
      -h|--help			This help
      

It's recommended to use _acsmdownloader.sh_ script to launch _acsmdownloader_ util



Sources
-------

Sources can be found @ http://indefero.soutade.fr/p/acsmdownloader



Copyright
---------

Grégory Soutadé



Licence
-------

GNU GPLv3
