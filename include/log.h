/*
  Copyright 2021 Grégory Soutadé

  This is a free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  It is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with it. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _LOG_H_
#define _LOG_H_

extern int verbose;

enum {
    ERROR,
    INFO,
    WARN,
    DEBUG,
    TRACE
};

#define LOG(lvl, msg) if (lvl <= verbose) {std::cout << msg << std::endl << std::flush;}

#define LOG_FUNC() LOG(TRACE, __FUNCTION__ << "() @ " << __FILE__ << ":" << __LINE__)

#endif
