/*
    File automatically generated by SOAdvancedDissector.py
    More information at http://indefero.soutade.fr/p/soadvanceddissector
*/

#ifndef _METRO_H
#define _METRO_H

#include <uft.h>


namespace metro {

    class WisDOM
    {
    public:
        void Create(uft::ErrorHandler*, int);

    };


}



#endif // _METRO_H