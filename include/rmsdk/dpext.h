/*
    File automatically generated by SOAdvancedDissector.py
    More information at http://indefero.soutade.fr/p/soadvanceddissector
*/

#ifndef _DPEXT_H
#define _DPEXT_H

namespace dpext {

    class LoadableDeviceInfo;
    class LoadableDeviceIteratorListener;


    class LoadableDeviceInfo
    {
    public:
    };

    class LoadableDeviceIteratorListener
    {
    public:
        virtual ~LoadableDeviceIteratorListener();
        virtual void virtfunc16() = 0;

    };


}



#endif // _DPEXT_H