/*
    File automatically generated by SOAdvancedDissector.py
    More information at http://indefero.soutade.fr/p/soadvanceddissector
*/

#ifndef _ADEPT_H
#define _ADEPT_H

// #include <dpio.h>
#include <uft.h>
#include <dp.h>
#include <dpdrm.h>
#include <dpsec.h>
#include <mdom.h>
#include <dpdoc.h>
#include <dputils.h>
#include <dpdev.h>
#include <dpcrypt.h>

class MockDevice;
class MetroWisDOM;

namespace adept {

    class ActivationData;
    class ActivationImpl;
    class ActivationList;
    class ActivationRecord;
    class ActivationServiceInfo;
    class Constraints;
    class ConsumableCount;
    class DRMProcessorImpl;
    class DRMProviderImpl;
    class FulfillmentItemData;
    class FulfillmentItemImpl;
    class FulfillmentItemList;
    class GPFile;
    class GPFileLock;
    class IdentityWrapper;
    class InfoSetSHA1Hasher;
    //class IoCallbackWrapper<adept::DRMProcessorImpl>;
    class License;
    class LicenseData;
    class LicenseImpl;
    class LicenseList;
    class LicenseRequestInfo;
    class LicenseServiceInfo;
    class LoanToken;
    class PermissionImpl;
    class PermissionList;
    class Permissions;
    class RightsImpl;
    class SyntheticRightsImpl;
    class UnverifiedRightsImpl;
    //class UrlLoader<adept::DRMProcessorImpl>;
    class User;


    class ActivationData
    {
    public:
        ActivationData();
        ActivationData(adept::ActivationData const&);

        ~ActivationData();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
        void toString() const;

        void* s_descriptor;

    };

    class ActivationImpl: public dpdrm::Activation
    {
    public:
        ActivationImpl(uft::sref<adept::ActivationData> const&);

        virtual ~ActivationImpl();
        virtual void addRef();
        virtual void release();
        virtual uft::String getUserID();
        virtual uft::String getDeviceID();
        virtual void getExpiration();
        virtual void getAuthority();
        virtual void getUsername();
        virtual bool hasCredentials();

    };

    class ActivationList: public dp::List<dpdrm::Activation>
    {
    public:
        ActivationList(uft::Vector const&);

        virtual ~ActivationList();
        virtual void addRef();
        virtual void release();
        virtual int length();
        virtual dp::ref<dpdrm::Activation> operator[](unsigned int);

    };

    class ActivationRecord
    {
    public:
        ActivationRecord();
        ActivationRecord(adept::ActivationRecord const&);

        ~ActivationRecord();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
        void toString() const;

        void* s_descriptor;

    };

    class ActivationServiceInfo
    {
    public:
        ActivationServiceInfo();
        ActivationServiceInfo(adept::ActivationServiceInfo const&);

        ~ActivationServiceInfo();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
        void toString() const;

        void* s_descriptor;

    };

    class Constraints
    {
    public:
        Constraints();
        Constraints(adept::Constraints const&);

        ~Constraints();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
        void toString() const;

        void* s_descriptor;

    };

    class ConsumableCount
    {
    public:
        ConsumableCount();

        ~ConsumableCount();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
        void toString() const;

        void* s_descriptor;

    };

    class DRMProcessorImpl: public dpdrm::DRMProcessor, public dpdoc::DocumentClient, public uft::ErrorHandler
    {
    public:
        DRMProcessorImpl(dpdrm::DRMProcessorClient*, dpdev::Device*);

	class DRMStep {};
	
        virtual ~DRMProcessorImpl();
        virtual void* getOptionalInterface(char const*);
        virtual void release();
        virtual dp::list<adept::ActivationImpl> getActivations();
        virtual void setUser(dp::String const&);
        virtual void setPartition(dpio::Partition*);
        virtual void reset();
        virtual void initWorkflows(unsigned int, dp::Data const&);
        virtual void initSignInWorkflow(unsigned int, dp::String const&, dp::String const&, dp::String const&);
        virtual void initSignInWorkflow(unsigned int, dp::String const&, dp::String const&, dp::Data const&);
        virtual void initLoanReturnWorkflow(dp::String const&);
        virtual void initUpdateLoansWorkflow(dp::String const&, dp::String const&);
        virtual void initJoinAccountsWorkflow(dp::String const&, dp::String const&, dp::String const&);
        virtual int startWorkflows(unsigned int);
        virtual void providePasshash(dp::Data const&);
        virtual void provideInput(dp::Data const&);
        virtual void provideConfirmation(dp::String const&, int);
        virtual void transferLoanTokensFrom(dpdev::Device*);
        virtual void transferCredentialsFrom(dpdev::Device*, dp::String const&, bool);
        virtual dp::list<dpdrm::FulfillmentItem> getFulfillmentItems();
        virtual void getFulfillmentID();
        virtual void isReturnable();
        virtual void addPasshash(dp::String const&, dp::Data const&);
        virtual void removePasshash(dp::String const&, dp::Data const&);
        virtual void calculatePasshash(dp::String const&, dp::String const&);
        virtual void getInterfaceVersion();
        virtual void getResourceStream(dp::String const&, unsigned int);
        virtual void canContinueProcessing(int);
        virtual void reportLoadingState(int);
        virtual void reportDocumentError(dp::String const&);
        virtual void reportErrorListChange();
        virtual void requestLicense(dp::String const&, dp::String const&, dp::Data const&);
        virtual void requestDocumentPassword();
        virtual void documentSerialized();
        virtual void reportStateError(uft::String const&);
        virtual void reportProcessError(uft::String const&);
        virtual void setURLString(uft::String const&);
        virtual uft::String getURLString();
        virtual void changeURL(uft::String const&);

        void activate();
        void activateErr(uft::String const&);
        void activateResp(uft::String const&, uft::Buffer const&);
        void activationIsLess(uft::sref<adept::ActivationData> const&, uft::sref<adept::ActivationData> const&);
        void addDeviceSections(mdom::Node const&);
        void addSignIn();
        void addSignInErr(uft::String const&);
        void addSignInResp(uft::String const&, uft::Buffer const&);
        void addSlashIfNeeded(uft::String const&);
        void authSignIn();
        void authSignInErr(uft::String const&);
        void authSignInResp(uft::String const&, uft::Buffer const&);
        void checkCurrentUserIsAuthorized();
        void createCertificate(dp::Data const&);
        void documentWithLicenseError(dp::String const&);
        void documentWithLicenseProgress(double);
        void documentWrittenWithLicense(dp::Unknown*, bool);
        void encryptWithDeviceKey(dp::Data const&);
        void endDownload();
        void finishFulfillmentResultProcessing();
        void finishWorkflow(int, bool, dp::Data const&);
        void fulfill();
        void fulfillErr(uft::String const&);
        void fulfillPasshash();
        void fulfillResp(uft::String const&, uft::Buffer const&);
        void getCredentialList();
        void getCredentialListErr(uft::String const&);
        void getCredentialListResp(uft::String const&, uft::Buffer const&);
        void getDefaultActivationForUser(uft::Vector const&, uft::String const&);
        void getIdentityByUser(uft::sref<adept::User> const&);
        void getNotifications(mdom::Node const&);
        void getReportedErrorString();
        void guessMimeType();
        void guessMimeTypeErr(uft::String const&);
        void guessMimeTypeResp(uft::String const&, uft::Buffer const&);
        void initAct();
        void initActErr(uft::String const&);
        void initActResp(uft::String const&, uft::Buffer const&);
        void initAuth();
        void initAuthErr(uft::String const&);
        void initAuthResp(uft::String const&, uft::Buffer const&);
        void initLicense();
        void initLicenseErr(uft::String const&);
        void initLicenseResp(uft::String const&, uft::Buffer const&);
        void initSignInWorkflowCommon(unsigned int);
        void joinAccounts();
        void joinAccountsErr(uft::String const&);
        void joinAccountsResp(uft::String const&, uft::Buffer const&);
        void licenseServiceInfo(uft::String const&);
        void licenseServiceInfoErr(uft::String const&);
        void licenseServiceInfoResp(uft::String const&, uft::Buffer const&);
        void makeSignInKey();
        void makeUserFromCredentials(mdom::Node const&, bool);
        void nextDownload();
        void nextFulfillmentItemNode();
        void nextNotification();
        void nextStep(adept::DRMProcessorImpl::DRMStep);
        void nextWorkflow();
        void notificationErr(uft::String const&);
        void notificationResp(uft::String const&, uft::Buffer const&);
        void operatorAuth();
        void operatorAuthErr(uft::String const&);
        void operatorAuthResp(uft::String const&, uft::Buffer const&);
        void processFulfillmentItemNode(mdom::Node const&);
        void processFulfillmentResult(mdom::Node const&, uft::String const&);
        void reportWorkflowError(unsigned int, dp::String const&);
        void resetWorkflowData();
        void returnLoan();
        void returnLoanErr(uft::String const&);
        void returnLoanResp(uft::String const&, uft::Buffer const&);
        void startActivationWorkflow();
        void startAddSignInWorkflow();
        void startAuthSignInWorkflow();
        void startDownloadWorkflow();
        void startFulfillmentWorkflow();
        void startGetCredentialListWorkflow();
        void startJoinAccountsWorkflow();
        void startLoanReturnWorkflow();
        void startNotificationWorkflow();
        void startUpdateLoansWorkflow();
        void tempFileError(dp::String const&);
        void tempFileProgress(double);
        void tempFileWritten(dp::Unknown*, bool);
        void uniqueFileCreated(dp::Unknown*, bool);
        void uniqueFileError(dp::String const&);
        void updateActivationServiceInfo(dpdev::Device*, uft::sref<adept::ActivationServiceInfo> const&);
        void updateLoans();
        void updateLoansErr(uft::String const&);
        void updateLoansResp(uft::String const&, uft::Buffer const&);

        void* ACTIVATION_SERVICE_URL;
        void* ADEPT_MIME_TYPE;
        void* REQ_ACTIVATE;
        void* REQ_ACTIVATION_SERVICE_INFO;
        void* REQ_ADD_SIGN_IN_DIRECT;
        void* REQ_AUTH;
        void* REQ_AUTHENTICATION_SERVICE_INFO;
        void* REQ_FULFILL;
        void* REQ_GET_CREDENTIAL_LIST;
        void* REQ_GET_LICENSE;
        void* REQ_INIT_LICENSE_SERVICE;
        void* REQ_JOIN_ACCOUNTS;
        void* REQ_LICENSE_SERVICE_INFO;
        void* REQ_LOAN_RETURN;
        void* REQ_SIGN_IN_DIRECT;

    };

    class DRMProviderImpl: public dpdrm::DRMProvider
    {
    public:
        DRMProviderImpl();

        virtual ~DRMProviderImpl();
        virtual DRMProcessorImpl* createDRMProcessor(dpdrm::DRMProcessorClient*, dpdev::Device*);
        virtual void parseLicense(dp::Data const&);

    };

    class FulfillmentItemData
    {
    public:
        FulfillmentItemData();
        FulfillmentItemData(adept::FulfillmentItemData const&);

        ~FulfillmentItemData();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
	uft::String toString() const;

        void* s_descriptor;

    };

    class FulfillmentItemImpl: public dpdrm::FulfillmentItem
    {
    public:
        FulfillmentItemImpl(uft::sref<adept::FulfillmentItemData> const&);

        virtual ~FulfillmentItemImpl();
        virtual void addRef();
        virtual void release();
        virtual dp::ref<dpdrm::Rights> getRights();
        virtual dp::String getMetadata(dp::String const&);
        virtual dp::String getDownloadMethod();
        virtual dp::String getDownloadURL();
        virtual dp::Data getPostData();

    };

    class FulfillmentItemList: public dp::List<dpdrm::FulfillmentItem>
    {
    public:
        FulfillmentItemList(uft::Vector const&);

        virtual ~FulfillmentItemList();
        virtual void addRef();
        virtual void release();
        virtual int length();
        virtual dp::ref<dpdrm::FulfillmentItem> operator[](unsigned int);

    };

    class GPFile
    {
    public:
        GPFile(uft::Buffer const&);

        ~GPFile();

        void assertLocked() const;
        void getCheckCode(bool);
        void getExtraFileInfo() const;
        void getPtr();
        void getRecordOffset(uft::Buffer const&, bool);
        void getWeakReferencePtr();
        void lock();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void resize(unsigned int, bool);
        void staticInit();
        void supportCheckCode();
        void toString() const;
        void unlock();

        void* s_descriptor;

    };

    class GPFileLock
    {
    public:
        GPFileLock(adept::GPFile*);

        ~GPFileLock();

    };

    class IdentityWrapper
    {
    public:
        IdentityWrapper();
        IdentityWrapper(adept::IdentityWrapper const&);

        ~IdentityWrapper();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
        void toString() const;

        void* s_descriptor;

    };

    class InfoSetSHA1Hasher: public mdom::InfoSetHasher
    {
    public:
        InfoSetSHA1Hasher();

        ~InfoSetSHA1Hasher();

        virtual void update(unsigned char const*, unsigned int);

        void getHash();

    };

    template<typename T>
    class IoCallbackWrapper: public dp::Callback
    {
    public:
        virtual ~IoCallbackWrapper();
        virtual void reportProgress(double);
        virtual void reportError(dp::String const&);
        virtual void invoke(dp::Unknown*);

	IoCallbackWrapper(T*, void (T::*)(dp::Unknown*, bool), void (T::*)(double), void (T::*)(dp::String const&));

    };

    class License
    {
    public:
        License(adept::License const&);
        License(uft::String const&, uft::String const&, uft::String const&, uft::String const&, uft::String const&, uft::String const&, uft::String const&, uft::String const&, uft::Buffer const&, uft::sref<adept::Permissions> const&, uft::String const&);
        License(uft::sref<adept::Permissions> const&, uft::String const&);

        ~License();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
        void toString() const;

        void* s_descriptor;

    };

    class LicenseData
    {
    public:
        LicenseData();
        LicenseData(adept::LicenseData const&);

        ~LicenseData();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
        void toString() const;

        void* s_descriptor;

    };

    class LicenseImpl: public dpdrm::License
    {
    public:
        LicenseImpl(uft::sref<adept::License> const&);

        virtual ~LicenseImpl();
        virtual void addRef();
        virtual void release();
        virtual void getUserID();
        virtual void getResourceID();
        virtual dp::String getVoucherID();
        virtual void getLicenseURL();
        virtual void getOperatorURL();
        virtual void getFulfillmentID();
        virtual void getDistributorID();
        virtual void getLicensee();
        virtual void getPermissions(dp::String const&);
        virtual void getCurrentCount(dp::String const&);
        virtual void consume(dp::String const&, int);
        virtual void getFlavor();

        void adjustCounts(uft::sref<adept::License> const&, int, int);
        void initializeLicenseCounts(uft::sref<adept::License> const&);

    };

    class LicenseList: public dp::List<dpdrm::License>
    {
    public:
        LicenseList(uft::Vector const&);

        virtual ~LicenseList();
        virtual void addRef();
        virtual void release();
        virtual int length();
        virtual dp::ref<dpdrm::License> operator[](unsigned int);

    };

    class LicenseRequestInfo
    {
    public:
        LicenseRequestInfo(adept::LicenseRequestInfo const&);
        LicenseRequestInfo(uft::String const&, uft::String const&, uft::Buffer const&);

        ~LicenseRequestInfo();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
        void toString() const;

        void* s_descriptor;

    };

    class LicenseServiceInfo
    {
    public:
        LicenseServiceInfo();
        LicenseServiceInfo(adept::LicenseServiceInfo const&);

        ~LicenseServiceInfo();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
        void toString() const;

        void* s_descriptor;

    };

    class LoanToken
    {
    public:
        LoanToken();
        LoanToken(adept::LoanToken const&);

        ~LoanToken();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
        void toString() const;

        void* s_descriptor;

    };

    class PermissionImpl: public dpdrm::Permission
    {
    public:
        PermissionImpl(uft::String const&, uft::sref<adept::Constraints> const&);

        virtual ~PermissionImpl();
        virtual void addRef();
        virtual void release();
        virtual void getPermissionType();
        virtual void getExpiration();
        virtual void getLoanID();
        virtual void getDeviceID();
        virtual void getDeviceType();
        virtual void getMaxResoultion();
        virtual void getParts();
        virtual void isConsumable();
        virtual void getInitialCount();
        virtual void getMaxCount();
        virtual void getIncrementInterval();

    };

    class PermissionList: public dp::List<dpdrm::Permission>
    {
    public:
        PermissionList(uft::String const&, uft::Vector const&);

        virtual ~PermissionList();
        virtual void addRef();
        virtual void release();
        virtual int length();
        virtual dp::ref<dpdrm::Permission> operator[](unsigned int);

    };

    class Permissions
    {
    public:
        Permissions();
        Permissions(adept::Permissions const&);

        ~Permissions();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
        void toString() const;

        void* s_descriptor;

    };

    class RightsImpl: public dpdrm::Rights
    {
    public:
        RightsImpl(dp::Data const&, uft::Vector const&);
        RightsImpl(mdom::Node const&, uft::Vector const&);

        virtual ~RightsImpl();
        virtual void addRef();
        virtual void release();
        virtual dp::list<dpdrm::License> getLicenses();
        virtual void getValidLicenses(dpdev::Device*);
        virtual dp::Data serialize();

        void getLicensesInternal(dpdev::Device*, bool);

    };

    class SyntheticRightsImpl: public dpdrm::Rights
    {
    public:
        SyntheticRightsImpl(uft::sref<adept::License> const&);

        virtual ~SyntheticRightsImpl();
        virtual void addRef();
        virtual void release();
        virtual dp::list<dpdrm::License> getLicenses();
        virtual void getValidLicenses(dpdev::Device*);
        virtual dp::Data serialize();

    };

    class UnverifiedRightsImpl: public adept::RightsImpl
    {
    public:
        UnverifiedRightsImpl(dp::Data const&, uft::Vector const&);
        UnverifiedRightsImpl(mdom::Node const&, uft::Vector const&);

        virtual ~UnverifiedRightsImpl();
        virtual void addRef();
        virtual void release();
        virtual dp::list<dpdrm::License> getLicenses();
        virtual void getValidLicenses(dpdev::Device*);
        virtual dp::Data serialize();

    };

    template<typename T> 
    class UrlLoader: public dpio::StreamClient, public dputils::GuardedObject
    {
    public:
        virtual ~UrlLoader();
        virtual void propertyReady(dp::String const&, dp::String const&);
        virtual void totalLengthReady(unsigned int);
        virtual void propertiesReady();
        virtual void bytesReady(unsigned int, dp::Data const&, bool);
        virtual void reportError(dp::String const&);
        virtual void deleteThis();

	UrlLoader(T*, void (T::*)(uft::String const&, uft::Buffer const&), void (T::*)(uft::String const&));

        void internalReportError(dp::String const&);
        void startReadingStream(dp::String const&, dp::String const&, unsigned int, dpio::Stream*, unsigned int);
        void startReadingStream(dpio::Stream*, unsigned int);

    };

    class User
    {
    public:
        User();
        User(adept::User const&);

        ~User();

        void getWeakReferencePtr();
        void* operator new(unsigned int, uft::Value&);
        void query(uft::Value const&, void*);
        void staticInit();
	dp::String toString() const;

        void* s_descriptor;

    };


    void addExpiration(mdom::Node&);
    void addNode(mdom::Node&, uft::QName const&, uft::Buffer const&);
    void addNode(mdom::Node&, uft::QName const&, uft::String const&);
    void addNode(mdom::Node&, uft::String const&, uft::Buffer const&);
    void addNode(mdom::Node&, uft::String const&, uft::String const&);
    void addNonce(mdom::Node&);
    void addSignature(mdom::Node&, dp::ref<dpcrypt::Identity>);
    void checkPartPermission(uft::String const&, uft::Value const&, int);
    void checkSignatureWithCert(uft::Buffer const&, uft::String const&, uft::Buffer const&, uft::Buffer const&, dpcrypt::Role);
    void checkSignatureWithCertList(uft::Vector const&, uft::String const&, uft::Buffer const&, uft::Buffer const&, dpcrypt::Role);
    void countPendingLicenseRequests(uft::Vector const&);
    void createActivationDOM(dpdev::Device*);
    void createDom(uft::String const&);
    void createLicenseDOM(uft::ErrorHandler*);
    void deriveKeyForPart(uft::Buffer const&, int);
    void derivePDFPartsKey(uft::Buffer const&, int*, unsigned int);
    void deviceVerify(unsigned char const*, unsigned char const*, unsigned char const*);
    void extractActivationData(mdom::Node const&);
    void extractActivationRecord(dpdev::Device*, bool, uft::ErrorHandler*);
    void extractActivationServiceInfo(mdom::Node const&);
    void extractConstraints(mdom::Node const&);
    void extractLicenseData(mdom::Node const&);
    void extractLicenseServiceInfo(mdom::Node const&);
    void extractLoanToken(mdom::Node const&);
    void extractUser(mdom::Node const&);
    void fillActivationServiceNode(mdom::Node&, uft::sref<adept::ActivationServiceInfo> const&);
    void fillUserNode(mdom::Node&, uft::sref<adept::User> const&);
    void findACS4ResourceIdFromVoucherId(mdom::Node const&, uft::String const&);
    void findAcsmNode(mdom::DOM*, unsigned int);
    void findAnonymousUser(uft::Dict const&);
    void findLicenseRequest(uft::Vector const&, uft::String const&, uft::String const&, bool);
    void findNode(mdom::Node const&, unsigned int, unsigned int, uft::String, unsigned int, uft::Buffer);
    void findNode(mdom::Node const&, unsigned int, unsigned int, unsigned int*, uft::String*, unsigned int, unsigned int*, uft::Buffer*);
    uft::sref<adept::User> findUserByName(uft::Dict const&, uft::String const&);
    void getActivationService(dpdev::Device*, uft::String const&);
    void getBase64EncodedContent(mdom::Node);
    uft::String getChildValue(mdom::Node const& node, unsigned int nodeType);
    void getLicense(mdom::Node const&, uft::String const&, uft::Vector const&, dpdev::Device*, uft::ErrorHandler*);
    void getLicenses(mdom::Node const&, uft::String const&, uft::Vector const&, dpdev::Device*, bool, uft::ErrorHandler*);
    uft::Dict getValidUsers(dpdev::Device*, uft::ErrorHandler*);
    void hashNode(mdom::Node);
    void isDeviceAvailable(dpdev::Device*);
    void managePasshash(dpdev::Device*, dp::String const&, dp::Data const&, bool);
    void mergeIn(mdom::Node const&, char const*, uft::ErrorHandler*);
    void mergeInLoanTokenNode(mdom::DOM*, uft::sref<adept::LoanToken> const&, mdom::Node const&);
    void nodeToString(mdom::Node const&);
    MetroWisDOM* parseXML(char const*);
    MetroWisDOM* parseXML(dp::Data const&);
    void removeChildren(mdom::Node const&, unsigned int, unsigned int, uft::String, unsigned int, uft::Buffer);
    void removeChildren(mdom::Node const&, unsigned int, unsigned int, unsigned int*, uft::String*, unsigned int, unsigned int*, uft::Buffer*);
    void renderFPH(uft::Buffer const&);
    void requestLicenses(uft::Vector const&, dpdoc::DocumentClient*);
    void scrambleActivation(mdom::Node const&, bool, dpdev::Device*);
    void scramblePrivateLicenseKey(uft::Buffer const&, bool, dpdev::Device*, bool*);

    void* ACS3CompatCertificate;
    void* ACS3CompatCertificateSize;
    void* ACS3CompatPrivateKey;
    void* ACS3CompatPrivateKeySize;

}



#endif // _ADEPT_H
