#!/bin/bash

ROOT=${PWD}
TARGET_DIR="${ROOT}/lib/rmsdk/${RMSDK_VERSION}"
TMP_DIR="${ROOT}/tmp"
# From https://wiki.mobileread.com/wiki/Kobo_Firmware_Releases#Firmware
KOBO_FIRMWARE="http://download.kobobooks.com/firmwares/kobo4/january2016/kobo-update-3.19.5761.zip"
# 2016 revision, same as Kobo firmware
OPENSSL="https://www.openssl.org/source/old/1.0.1/openssl-1.0.1u.tar.gz"


[ -e ${TMP_DIR} ] && rm -rf ${TMP_DIR}
mkdir ${TMP_DIR}
pushd ${TMP_DIR}

echo "Download Kobo firmware..."

curl ${KOBO_FIRMWARE} -o kobo_firmware.zip || exit 1

echo "Uncompress firmware"

unzip kobo_firmware.zip || exit 1

tar -zxvf KoboRoot.tgz

echo "Extract libraries"

${ROOT}/scripts/find_libs.sh -q -C -t "${TMP_DIR}/usr/local/Kobo/librmsdk.so" -l "${TMP_DIR}" -o "${TARGET_DIR}"
${ROOT}/scripts/find_libs.sh -q -C -t "${TMP_DIR}/usr/local/Trolltech/QtEmbedded-4.6.2-arm/lib/libQtCore.so.4.6.2" -l "${TMP_DIR}" -o "${TARGET_DIR}"
${ROOT}/scripts/find_libs.sh -q -C -t "${TMP_DIR}/usr/local/Trolltech/QtEmbedded-4.6.2-arm/lib/libQtNetwork.so.4.6.2" -l "${TMP_DIR}" -o "${TARGET_DIR}"

echo "Fix libraries"

pushd "${TARGET_DIR}"
ln -s libQtCore.so.4.6.2 libQt5Core.so.5
ln -s libQt5Core.so.5 libQt5Core.so
ln -s libQtNetwork.so.4.6.2 libQt5Network.so.5
ln -s libQt5Network.so.5 libQt5Network.so
ln -s librmsdk.so.1.0.0 librmsdk.so.1
rm -f libgcc* libstdc++*
popd

if [ -z "${NO_BUILD_OPENSSL}" -o ${NO_BUILD_OPENSSL} -eq 0 ] ; then

    echo "Downloading OpenSSL"
    
    curl ${OPENSSL} -o openssl.tgz || exit 1
    
    echo "Uncompress OpenSSL"

    tar -zxvf openssl.tgz
    
    pushd openssl-1.0.1u

    echo "Compile OpenSSL"

    ./Configure linux-armv4 shared --prefix="${PWD}/root" || exit 1

    make CC=${CC} -j4 build_libs || exit 1

    echo "Install OpenSSL"

    rm -f ${TARGET_DIR}/libcrypto* ${TARGET_DIR}/libssl*

    cp --no-dereference libcrypto.so* libssl.so* ${TARGET_DIR}
    
    popd
fi

popd

echo "Cleaning tmp dir"

rm -rf ${TMP_DIR}
